import numpy as np
import element
import universalElement as ue

class Jakobian:

    def __init__(self, newElement: element.Element, newUniEl: ue.UniversalElement, idIntegralPoint: int):
        self.detJ = 0.0
        self.jakobian = np.zeros((2, 2))
        self.invjakobian = np.zeros((2, 2))
        self.jakobian[0, 0] = newUniEl.dNdKsi[idIntegralPoint, 0] * newElement.leftDown.x + \
                              newUniEl.dNdKsi[idIntegralPoint, 1] * newElement.rightDown.x + \
                              newUniEl.dNdKsi[idIntegralPoint, 2] * newElement.rightUp.x +\
                              newUniEl.dNdKsi[idIntegralPoint, 3] * newElement.leftUp.x

        self.jakobian[0, 1] = newUniEl.dNdKsi[idIntegralPoint, 0] * newElement.leftDown.y + \
                              newUniEl.dNdKsi[idIntegralPoint, 1] * newElement.rightDown.y + \
                              newUniEl.dNdKsi[idIntegralPoint, 2] * newElement.rightUp.y + \
                              newUniEl.dNdKsi[idIntegralPoint, 3] * newElement.leftUp.y

        self.jakobian[1, 0] = newUniEl.dNdEta[idIntegralPoint, 0] * newElement.leftDown.x + \
                              newUniEl.dNdEta[idIntegralPoint, 1] * newElement.rightDown.x + \
                              newUniEl.dNdEta[idIntegralPoint, 2] * newElement.rightUp.x + \
                              newUniEl.dNdEta[idIntegralPoint, 3] * newElement.leftUp.x

        self.jakobian[1, 1] = newUniEl.dNdEta[idIntegralPoint, 0] * newElement.leftDown.y + \
                              newUniEl.dNdEta[idIntegralPoint, 1] * newElement.rightDown.y + \
                              newUniEl.dNdEta[idIntegralPoint, 2] * newElement.rightUp.y + \
                              newUniEl.dNdEta[idIntegralPoint, 3] * newElement.leftUp.y

        self.detJ = np.linalg.det(self.jakobian)
        self.invjakobian = np.linalg.inv(self.jakobian)