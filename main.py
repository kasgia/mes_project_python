import grid
import globalData as gd
import numpy as np
import matrixCalc as mc
import matplotlib.pyplot as mp

if __name__ == '__main__':
    datas = gd.GlobalElement()
    myGrid = grid.Grid()
    myGrid.setNodes(datas)
    myGrid.setElements(datas)
    Tnext = np.dot(datas.initTemp, np.ones((1, myGrid.countNodes)))
    myGrid.setTempNodes(Tnext)

    for i in range(0, int(datas.time/datas.stepTime)):
        mc.calcLocalMatrixH(myGrid, datas)
        mc.agregation(myGrid)
        tmp = np.dot(1/datas.stepTime, myGrid.globalCMatrix)
        myHCdT = np.add(myGrid.globalHMatrix, tmp)
        tmp1 = np.dot(Tnext, tmp)
        myCdTP = np.add(myGrid.globalPMatrix, tmp1)
        Tnext = np.linalg.solve(myHCdT, np.transpose(myCdTP))
        Tnext = np.transpose(Tnext)
        myGrid.setTempNodes(Tnext)
        print('Czas {} - min {}, max = {}'.format((i+1) * datas.stepTime, min(Tnext[0]), max(Tnext[0])))

    mp.pcolor(np.reshape(Tnext, (datas.countWidth, datas.countHigh)))
    mp.show()