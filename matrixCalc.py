import grid
import globalData as gd
import numpy as np
import jakobian as jb
import universalElement as ue

def calcLocalMatrixH(myGrid: grid.Grid, myData: gd.GlobalElement):
    K = myData.K
    alfa = myData.alfa
    Ro = myData.Ro
    C = myData.c
    newUniEl = ue.UniversalElement(myData)
    lengthH = 0.5 * myData.high/(myData.countHigh - 1)
    lengthW = 0.5 * myData.width/(myData.countWidth - 1)
    detJB = [lengthW, lengthW, lengthH, lengthH, lengthW, lengthW, lengthH, lengthH]

    for currentElement in myGrid.elements:
        for j in range(0, myData.countOfIntegrationsPoints**2):
            N = np.zeros((1, 4))
            dNdx = np.zeros((1, 4))
            dNdy = np.zeros((1, 4))
            jakobian = jb.Jakobian(currentElement, newUniEl, j)

            for k in range(0, 4):
                dNdx[0, k] = (jakobian.invjakobian[0, 0] * newUniEl.dNdKsi[j, k] + \
                          jakobian.invjakobian[0, 1] * newUniEl.dNdEta[j, k])
                dNdy[0, k] = (jakobian.invjakobian[1, 0] * newUniEl.dNdKsi[j, k] + \
                          jakobian.invjakobian[1, 1] * newUniEl.dNdEta[j, k])
                N[0, k] = newUniEl.shapeFunction[j, k]

            temp1 = np.dot(np.transpose(dNdx), dNdx) * jakobian.detJ
            temp2 = np.dot(np.transpose(dNdy), dNdy) * jakobian.detJ
            temp3 = np.add(temp1, temp2)
            temp4 = np.dot(temp3, K)
            currentElement.matrixH = np.add(currentElement.matrixH, temp4)

            temp5 = np.dot(np.transpose(N), N)
            temp6 = Ro*C*jakobian.detJ
            temp7 = np.dot(temp6, temp5)
            currentElement.matrixC = np.add(currentElement.matrixC, temp7)


        for i in range(0, 8):
            NBoarder = np.zeros((1, 4))
            for k in range(0, 4):
                NBoarder[0, k] = newUniEl.shapeEdge[i, k]
            temp8 = np.dot(np.transpose(NBoarder), NBoarder)
            temp9 = detJB[i] * alfa
            temp10 = temp8 * temp9

            temp12 = temp9 * myData.ambientTemp
            temp13 = np.dot(temp12, NBoarder)

            if i==0 or i == 1:
                temp11 = np.dot(currentElement.edge[0, 0], temp10)
                temp14 = np.dot(currentElement.edge[0, 0], temp13)
            if i==2 or i == 3:
                temp11 = np.dot(currentElement.edge[0, 1], temp10)
                temp14 = np.dot(currentElement.edge[0, 1], temp13)
            if i==4 or i == 5:
                temp11 = np.dot(currentElement.edge[0, 2], temp10)
                temp14 = np.dot(currentElement.edge[0, 2], temp13)
            if i==6 or i == 7:
                temp11 = np.dot(currentElement.edge[0, 3], temp10)
                temp14 = np.dot(currentElement.edge[0, 3], temp13)

            currentElement.matrixH = np.add(currentElement.matrixH, temp11)

            currentElement.P = np.add(currentElement.P, temp14)
    return

def agregation(myGrid: grid.Grid):
    globalCMatrix = np.zeros((myGrid.countNodes, myGrid.countNodes))
    globalHMatrix = np.zeros((myGrid.countNodes, myGrid.countNodes))
    globalPMatrix = np.zeros((1, myGrid.countNodes))
    for elem in myGrid.elements:
        for i in range(0, 4):
            globalPMatrix[0, elem.vectorID[0, i]] += elem.P[0, i]
            for j in range(0, 4):
                globalHMatrix[elem.vectorID[0, i]][elem.vectorID[0, j]] += elem.matrixH[i, j]
                globalCMatrix[elem.vectorID[0, i]][elem.vectorID[0, j]] += elem.matrixC[i, j]

    myGrid.globalCMatrix = globalCMatrix
    myGrid.globalHMatrix = globalHMatrix
    myGrid.globalPMatrix = globalPMatrix