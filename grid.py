import element as el
import globalData as gd
import node as nd
import numpy as np

class Grid:
    def __init__(self):
        self.countElement = 0
        self.countNodes = 0
        self.elements = []
        self.nodes = []
        self.globalHMatrix = np.zeros((self.countNodes, self.countNodes))
        self.globalCMatrix = np.zeros((self.countNodes, self.countNodes))
        self.globalPMatrix = np.zeros((1, self.countNodes))

    def setNodes(self, newdata: gd.GlobalElement):
        self.countNodes = newdata.howManyAllNodes
        stepHigh = newdata.high/(newdata.countHigh - 1)
        stepWidth = newdata.width/(newdata.countWidth - 1)
        k = 0
        l = 0
        j = 0
        for i in range(0, self.countNodes):
            nodeToAppend = nd.Node(i, 0, 0, 0)
            self.nodes.append(nodeToAppend)
        for node in self.nodes:
            if k == newdata.countHigh:
                k = 0
            node.y = k*stepHigh
            k+=1
            if (j == newdata.countHigh):
                l+=1
                j = 0
            node.x = l * stepWidth
            j+=1
            node.isEdge = node.isBoarder(newdata)

    def printNodes(self):
        for i in self.nodes:
            i.printNode()

    def setElements(self, newdata: gd.GlobalElement):
        liczbaElementow = 0
        for i in range(0, newdata.countWidth - 1):
            for j in range(0, newdata.countHigh - 1):
                element = el.Element(liczbaElementow, self.nodes[i * newdata.countHigh + j],
                                  self.nodes[(i + 1) * newdata.countHigh + j],
                                  self.nodes[(i + 1) * newdata.countHigh + j + 1],
                                  self.nodes[i * newdata.countHigh + j + 1])
                element.vectorID[0, 0] = self.nodes[i * newdata.countHigh + j].idNode
                element.vectorID[0, 1] = self.nodes[(i + 1) * newdata.countHigh + j].idNode
                element.vectorID[0, 2] = self.nodes[(i + 1) * newdata.countHigh + j + 1].idNode
                element.vectorID[0, 3] = self.nodes[i * newdata.countHigh + j + 1].idNode
                element.setEdges()
                self.elements.append(element)
                liczbaElementow += 1

    def printElements(self):
        for el in self.elements:
            el.printElement()

    def setTempNodes(self, vectTemp: []):
        j = 0
        for i in self.nodes:
            i.t = vectTemp[0, j]
            j += 1
