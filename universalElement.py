import globalData as gd
import numpy as np

class Points:
    def __init__(self, newKsi: float, newEta: float):
        self.ksi = newKsi
        self.eta = newEta

class UniversalElement:

    def __init__(self, newData: gd.GlobalElement):
        self.countOfIntegrationPoints = 0
        self.coordinateIntegrationPoints = []
        self.coordinateIntegrationPointsBoarder = []
        self.dNdEta = np.zeros((4, 4))
        self.dNdKsi = np.zeros((4, 4))
        self.shapeFunction = np.zeros((4, 4))
        self.shapeEdge = np.zeros((8, 4))
        self.countOfIntegrationPoints = newData.countOfIntegrationsPoints
        if self.countOfIntegrationPoints == 2:
            value = 3 ** (-0.5)
            self.coordinateIntegrationPoints.append(Points(-value, -value))
            self.coordinateIntegrationPoints.append(Points(value, -value))
            self.coordinateIntegrationPoints.append(Points(value, value))
            self.coordinateIntegrationPoints.append(Points(-value, value))
            for i in range(0, 4):
                self.dNdEta[i, 0] = -0.25 * (1 - self.coordinateIntegrationPoints[i].ksi)
                self.dNdEta[i, 1] = -0.25 * (1 + self.coordinateIntegrationPoints[i].ksi)
                self.dNdEta[i, 2] = 0.25 * (1 + self.coordinateIntegrationPoints[i].ksi)
                self.dNdEta[i, 3] = 0.25 * (1 - self.coordinateIntegrationPoints[i].ksi)

                self.dNdKsi[i, 0] = -0.25 * (1 - self.coordinateIntegrationPoints[i].eta)
                self.dNdKsi[i, 1] = 0.25 * (1 - self.coordinateIntegrationPoints[i].eta)
                self.dNdKsi[i, 2] = 0.25 * (1 + self.coordinateIntegrationPoints[i].eta)
                self.dNdKsi[i, 3] = -0.25 * (1 + self.coordinateIntegrationPoints[i].eta)

                self.shapeFunction[i, 0] = 0.25 * (1 - self.coordinateIntegrationPoints[i].ksi) * \
                                           (1 - self.coordinateIntegrationPoints[i].eta)
                self.shapeFunction[i, 1] = 0.25 * (1 + self.coordinateIntegrationPoints[i].ksi) *\
                                           (1 - self.coordinateIntegrationPoints[i].eta)
                self.shapeFunction[i, 2] = 0.25 * (1 + self.coordinateIntegrationPoints[i].ksi) * \
                                           (1 + self.coordinateIntegrationPoints[i].eta)
                self.shapeFunction[i, 3] = 0.25 * (1 - self.coordinateIntegrationPoints[i].ksi) * \
                                           (1 + self.coordinateIntegrationPoints[i].eta)

            self.coordinateIntegrationPointsBoarder.append(Points(-value, -1))
            self.coordinateIntegrationPointsBoarder.append(Points(value, -1))
            self.coordinateIntegrationPointsBoarder.append(Points(1, -value))
            self.coordinateIntegrationPointsBoarder.append(Points(1, value))
            self.coordinateIntegrationPointsBoarder.append(Points(value, 1))
            self.coordinateIntegrationPointsBoarder.append(Points(-value, 1))
            self.coordinateIntegrationPointsBoarder.append(Points(-1, value))
            self.coordinateIntegrationPointsBoarder.append(Points(-1, -value))

            for i in range(0, 8):
                self.shapeEdge[i, 0] = 0.25 * (1 - self.coordinateIntegrationPointsBoarder[i].ksi) * \
                                       (1 - self.coordinateIntegrationPointsBoarder[i].eta)
                self.shapeEdge[i, 1] = 0.25 * (1 + self.coordinateIntegrationPointsBoarder[i].ksi) * \
                                       (1 - self.coordinateIntegrationPointsBoarder[i].eta)
                self.shapeEdge[i, 2] = 0.25 * (1 + self.coordinateIntegrationPointsBoarder[i].ksi) * \
                                       (1 + self.coordinateIntegrationPointsBoarder[i].eta)
                self.shapeEdge[i, 3] = 0.25 * (1 - self.coordinateIntegrationPointsBoarder[i].ksi) * \
                                       (1 + self.coordinateIntegrationPointsBoarder[i].eta)
