import globalData as gd

class Node:

    def __init__(self, newid: int, newx: float, newy: float, newt: float):
        self.idNode = newid
        self.x = newx
        self.y = newy
        self.t = newt
        self.isEdge = False

    def __str__(self):
        print("Node ID[{}]: x = {}, y = {}, t = {}, edge = {}".format(self.idNode, self.x, self.y, self.t, self.isEdge))

    def isBoarder(self, newData: gd.GlobalElement):
        if self.y == 0.0 or self.y == newData.high or self.x == 0.0 or self.x == newData.width:
            return True
        return False