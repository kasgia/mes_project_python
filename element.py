import node
import numpy as np
import globalData as gd

class Element:

    def __init__(self, newid: int, newld: node.Node, newrd: node.Node, newru: node.Node, newlu: node.Node):
        self.idelement = newid
        self.leftDown = newld
        self.rightDown = newrd
        self.rightUp = newru
        self.leftUp = newlu
        self.matrixH = np.zeros((4, 4))
        self.matrixC = np.zeros((4, 4))
        self.vectorID = np.zeros((1, 4), int)
        self.edge = np.zeros((1, 4))  # [dół prawo góra lewo]
        self.P = np.zeros((1, 4))

    def setEdges(self):
        if self.leftDown.isEdge == True and self.rightDown.isEdge == True:
            self.edge[0, 0] = 1
        if self.rightUp.isEdge == True and self.rightDown.isEdge == True:
            self.edge[0, 1] = 1
        if self.leftUp.isEdge == True and self.rightUp.isEdge == True:
            self.edge[0, 2] = 1
        if self.leftDown.isEdge == True and self.leftUp.isEdge == True:
            self.edge[0, 3] = 1

    def printElement(self):
        print("Element[{}]: ".format(self.idelement))
        self.leftDown.__str__()
        self.rightDown.__str__()
        self.rightUp.__str__()
        self.leftUp.__str__()
        print(self.vectorID)
        print(self.edge)

