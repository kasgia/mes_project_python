import json

class GlobalElement:
    high = 0.0
    width = 0.0
    countHigh = 0
    countWidth = 0
    countOfIntegrationsPoints = 0
    howManyAllNodes = 0
    howManyElements = 0
    K = 0.0
    alfa = 0.0
    Ro = 0.0
    c = 0.0
    ambientTemp = 0.0
    initTemp = 0.0
    time = 0.0
    stepTime = 0

    def __init__(self):
        with open("startData.json") as file:
            myfile = json.load(file)
            self.high = myfile['high']
            self.width = myfile['width']
            self.countHigh = myfile['countHigh']
            self.countWidth = myfile['countWidth']
            self.countOfIntegrationsPoints = myfile['countOfIntegrationPoints']
            self.howManyAllNodes = self.countWidth * self.countHigh
            self.howManyElements = (self.countHigh - 1) * (self.countWidth - 1)
            self.K = myfile['K']
            self.alfa = myfile['alfa']
            self.Ro = myfile['Ro']
            self.c = myfile['specificHeat']
            self.ambientTemp = myfile['ambientTemp']
            self.initTemp = myfile['initTemp']
            self.time = myfile['time']
            self.stepTime = myfile['stepTime']

    def printGlobalElement(self):
        print("High = {}".format(self.high))
        print("Width = {}".format(self.width))
        print("count of high = {}".format(self.countHigh))
        print("count of width = {}".format(self.countWidth))
        print("count of integration points = {}".format(self.countOfIntegrationsPoints))
        print("count of all nodes = {}".format(self.howManyAllNodes))
        print("count of elements = {}".format(self.howManyElements))
        print("K = {}".format(self.K))
        print("alfa = {}".format(self.alfa))
        print("Ro = {}".format(self.Ro))
        print("c = {}".format(self.c))
        print("Ambient temperature = {}".format(self.ambientTemp))
        print("Init temperature = {}".format(self.initTemp))
        print("Time = {}".format(self.time))
        print("Step time = {}".format(self.stepTime))